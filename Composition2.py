from __future__ import division
from abc import *
from numpy import *
from collections import defaultdict

class Classifier(metaclass=ABCMeta):

    _tags = []
    _name = None
    _core = None
    _extractor = None
    def __init__(self, name, tags, core, feature_extractor):
        self._tags = tags
        self._name = name
        self._core = core
        self._extractor = feature_extractor

    @property
    def tags(self):
        return self._tags

    @property
    def name(self):
        return self._name

    @abstractmethod
    def build(self, training_set):
        if self._core != None:
            self._core.build(training_set)
        return self

    @abstractmethod
    def classify(self, text):
        if self._core != None:
            return self._core.classify(text)


    def error(self, expert_tag, result_tag):
        return expert_tag != result_tag

    def normalize_accuracy(self, accuracy, label, length):
        return accuracy[label] / length

    def classify_set(self, text_set):
        result = []
        accuracy = { 'true-positive': 0.0, #relation true-literature to all texts
             'true-negative': 0.0, #relation true-non-literature to all texts
             'false-positive': 0.0, #relation false-literature to all texts
             'false-negative': 0.0 } #relation false classified non literature to all texts
        for text in text_set:
            result_tag = self.classify(text)
            result.append((text, result_tag))
            error = self.error(expert_tag=text['expert_tag']['label'], result_tag=result_tag)

            if result_tag == self._tags[0].label:
                if not error:
                    accuracy['true-positive'] += 1
                else:
                    accuracy['false-positive'] += 1
            else:
                if not error:
                    accuracy['true-negative'] += 1
                else:
                    accuracy['false-negative'] += 1

        return result, accuracy

class BinaryClassifier(Classifier):

    def __init__(self, name, pos_tag, neg_tag, core):
        super().__init__(name=name, tags=[pos_tag,neg_tag], core=core, feature_extractor=None)

    def sign(self, label):
        return 1 if label == self.pos_tag.label else -1

    def build(self, training_set):
        self._core.build(training_set)

    def classify(self, text):
        return self._core.classify(text)

    @property
    def pos_tag(self):
        return self._tags[0]

    @pos_tag.setter
    def pos_tag(self, value):
        self._pos_tag = value

    @property
    def neg_tag(self):
        return self._tags[1]

    @neg_tag.setter
    def neg_tag(self, value):
        self._neg_tag = value



class NaiveBayes(Classifier):
    __classes = defaultdict(lambda :0)
    __featureFreq = defaultdict(lambda :0)

    def __init__(self, name, feature_extractor):
        super(NaiveBayes, self).__init__(tags=None, name=name, core=None, feature_extractor=feature_extractor)

    def build(self, training_set):
        self.__classes.clear()
        self.__featureFreq.clear()

        if training_set == None:
            return

        classes, featureFreq = defaultdict(lambda:0), defaultdict(lambda:0)
        for text_item in training_set:
            classes[text_item['expert_tag']['label']] += 1

            # count classes frequencies
            for feat in self._extractor.extract(text_item):
                featureFreq[text_item['expert_tag']['label'], feat] += 1          # count features frequencies

        self._tags = classes.keys()

        for label, feat in featureFreq:                # normalize features frequencies
            featureFreq[label, feat] /= classes[label]

        for c in classes:                       # normalize classes frequencies
            classes[c] /= len(training_set)

        self.__classes = classes
        self.__featureFreq = featureFreq
        return self
        # return P(C) and P(O|C)

    def classify(self, text):
        return min(self.__classes.keys(),              # calculate argmin(-log(C|O))
            key = lambda cl: -log(self.__classes[cl]) + \
                sum(-log(self.__featureFreq.get((cl,feat), 10**(-7))) for feat in self._extractor.extract(text)))



class ClassifiersComposition(BinaryClassifier):
    __N = 0
    __weights = None
    __ALPHA = []

    def __init__(self, name, pos_tag, neg_tag, core):
        super().__init__(pos_tag=pos_tag, name=name, neg_tag=neg_tag, core=core)

    def __classify(self, item, cls):
        tag = cls.classify(item)
        return cls.sign(tag)

    def build(self, training_set): #test=False

        self.__N = len(training_set)
        self.__weights = ones(self.__N)/self.__N

        if self._core == None or len(self._core) < 1:
            return

       # self.__pos_tag# = self._core[0].pos_tag
       # self.__neg_tag = self._core[0].neg_tag

        errors = []

        for rule in self._core:
           # for text_item in training_set:
            #    label_tag = rule.classify(text_item)
           #     errors.append(self.sign(label_tag) != self.sign(text_item['expert_tag']['label']))
            errs = []
            for text_item in training_set:
                tag = rule.classify(text_item)
                expert_label = text_item['expert_tag']['label']
                errs.append(tag != expert_label)
            errors = array(errs)
            #errors = array([1 if t[1] == "l" else -1 != self.__cls_map(t[0], cls) for t in self.training_set])
            e = (errors*self.__weights).sum()
          #  if test: return e
            alpha = 0.5 * log((1-e)/e)
           # print ('e=%.2f a=%.2f'%(e, alpha))
            w = zeros(self.__N)
            for i in range(self.__N):
                if errors[i] == 1:
                    w[i] = self.__weights[i] * exp(alpha)
                else:
                    w[i] = self.__weights[i] * exp(-alpha)

            self.__weights = w / w.sum()
            self.__ALPHA.append(alpha)
        return self

    def classify(self, text):
        NR = len(self._core)

        #for (name, x, label) in test_set:
        hx = []
        i = 0
        for cls in self._core:
            tag = cls.classify(text)
            hx.append(self.__ALPHA[i]* cls.sign(tag))
            i+=1

        result_tag = self.pos_tag.label if sum(hx) > 0 else self.neg_tag.label
        return result_tag


"""
class SimpleSubjectAreaClassifier(BinaryClassifier):

    def __init__(self, pos_tag, neg_tag, feature_extractor):
        tags = [pos_tag, neg_tag]
        core = ClassifiersComposition(pos_tag=pos_tag, neg_tag=neg_tag, feature_extractor=feature_extractor)
        super().__init__(tags=tags, core=core)
        self.pos_tag = pos_tag
        self.neg_tag = neg_tag

class ComplexClassifier(ClassifiersComposition):

    __rules = []

    def __init__(self, pos_tag, neg_tag, tag_classifiers, area_classifier):
        super().__init__(pos_tag=pos_tag, neg_tag=neg_tag)
        if tag_classifiers != None and len(tag_classifiers) > 0:
            self.__rules += tag_classifiers
        if area_classifier != None:
            self.__rules += [area_classifier]


    def build(self, training_set):
        super().build(training_set, self.__rules)
        return self"""
