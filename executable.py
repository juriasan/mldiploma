from TextDataHandlers import *
from DocumentModel import  *
from Composition2 import *
from DatabaseConnector import *

feature_groups = [{ 'name': 'literature', 'feats': list(['книга', 'роман',
                        'писатель',
                        'литература',
                        'автор',
                        'литературный',
                        'премия',
                        'книжный',
                        'написать',
                        'читать',
                        'писать',
                        'проза',
                        'читатель',
                        'поэт',
                        'рассказ',
                        'том',
                        'издательство',
                        'журнал',
                        'статья',
                        'издание',
                        'сборник',
                        'лауреат'])}, { 'name': 'time', 'feats': list([ 'весь',
                            'год',
                            'человек',
                            'время',
                            'стать',
                            'новый',
                            'имя',
                            'знать',
                            'история',
                            'жизнь',
                            'мир',
                            'россия',
                            'друг',
                            'вопрос',
                            'современный',
                            'смысл',
                            'век',
                            'страница',
                            'бог',
                            'свет',
                            'выйти',
                            'эпоха',
                            'интеллектуал',
                            'всегда',
                            'жить'])}, {'name': 'science', 'feats': list([
                                   'русский',
                              'язык',
                              'слово',
                              'фонема',
                              'говорить',
                              'главный',
                               'речь',
                               'чередование',
                               'наука',
                               'известный',
                               'теория',
                               'начало',
                               'перевод',
                               'данный',
                             'понятие',
                             'найти',
                             'список',
                             'пример'
    ])}, {'name': 'plot', 'feats': list([
                     'позиция',
                    'образ',
                    'герой',
                    'конец',
                    'сюжет',
                    'текст',
                    'взгляд',
                    'название',
                    'фантастика',
                    'важный',
                    'отношение',
                    'критика',
                    'вариант',
                    'качество',
                    'происходить',
                    'роль',
                    'понимать',
                    'цитата',
                    'момент'
    ])}]

def feature_groups_save_to_db(feature_groups):
    dbconnector = MongoDbConnector(host='localhost',port=27017,db_name='ml_diploma')

    if dbconnector.connect():
        dbconnector.db['feature_groups'].drop()
        for group in feature_groups:
            dbconnector.db['feature_groups'].insert_one(group)


def db_fill():
    tags =  {}

    open_corpora_core = CorpusReader('annot.opcorpora.xml')
    lit_tags = ['Тема:ВикиПортал:Лингвистика','Тема:ЧасКор:Книги*','Тема:ЧасКор:Культура/Литература']
    lit_tag = Tag("literature")
    non_lit_tag = Tag("non-literature")

    morph_analyzer = MorphAnalyzer()
    dbconnector = MongoDbConnector(host='localhost',port=27017,db_name='ml_diploma')
    dbconnector.connect()
    dbrepository = TextDataSetRepository("literature_texts", dbconnector)

    def document_model_prototype ():
        return BagOfWords()

    lit_full_data_set = OpenCorporaDataSet(core=open_corpora_core, document_model=BagOfWords())\
        .extract_data(expert_tag=lit_tag,
                      tags=lit_tags)\
        .handle_words(morph_analyzer)\
        .build(document_model_prototype)\
        .save(dbrepository, True)

    dbrepository.entity_name = "literature_texts_data_set"
    lit_full_data_set.save_set(dbrepository, True)

    dbrepository.entity_name = "non_literature_texts"

    non_lit_full_data_set = OpenCorporaDataSet(core=open_corpora_core, document_model=BagOfWords())\
        .extract_data(expert_tag=non_lit_tag,  tags=['Тема:ВикиКатегория:Автомобили',
                                    'Тема:ЧасКор:В мире',
                                    'Тема:ВикиКатегория:Политика',
                                    'Тема:ЧасКор:Здоровье',
                                    'Тема:ЧасКор:Экономика',
                                    'Тема:ЧасКор:Медиа',
                                    'Тема:ВикиКатегория:NASA',
                                    'Тема:ВикиКатегория:Спорт',
                                    'Тема:ЧасКор:Технологии',
                                    'Тема:ВикиКатегория:Футбол',
                                    'Тема:ВикиКатегория:Преступность и право'
                                    ])\
        .handle_words(morph_analyzer)\
        .build(document_model_prototype)\
        .save(dbrepository, True)

    dbrepository.entity_name = "non_literature_texts_data_set"
    non_lit_full_data_set.save_set(dbrepository, True)

    dbconnector.db['intern_tags'].drop()
    dbconnector.db['intern_tags'].insert_many([{'label': tag} for tag in lit_tags])

    dbconnector.close()

def features_top(dbconnector, item_getter_callback):

    items = item_getter_callback(dbconnector.db)
    features_top = [item[0] for item in sorted(items,
                          key=operator.itemgetter(1),reverse=True)[:200]]
    #for f in features_top:
    #    print(f + "\n", end=" ")

   # print(" ")
    return features_top

def get_dataset_from_file():

    lit_tags = ['Тема:ВикиПортал:Лингвистика','Тема:ЧасКор:Книги*','Тема:ЧасКор:Культура/Литература']
    lit_tag = Tag("literature")
    non_lit_tag = Tag("non-literature")

    morph_analyzer = MorphAnalyzer()
    dbconnector = MongoDbConnector(host='localhost',port=27017,db_name='ml_diploma')
    dbconnector.connect()
    dbrepository = TextDataSetRepository("literature_texts", dbconnector)

    def document_model_prototype ():
        return BagOfWords()

    lit_texts = FileDataSet(path_to_dir=os.path.abspath("texts/literature"), document_model=BagOfWords())\
            .extract_data(lit_tag)\
            .handle_words(morph_analyzer)\
            .build(document_model_prototype)\

get_dataset_from_file()