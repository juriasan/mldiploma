import unittest
from texts_handler.features import *
from DocumentModel import *
from DatabaseConnector import  *
from Composition2 import *
from TextDataHandlers import *
import opencorpora
from FeatureExtractor import *
class TestUpdateFeatureGroups(unittest.TestCase):

    def test_build_model(self):
        model = BagOfWords()
        model.form_model(['sss', 'dddd', 'uuu'])

    def test_col(self):

        col1 = [1, 2, 3]
        col2 = [4, 5, 6]

        print ('before\n')
        print(col1, '\n')
        print(col2, '\n')
        print('after\n')

        col1 += col2

        col1.append(111)

        print(col1, '\n')
        print(col2, '\n')

    def test_opencorpora(self):
        c = opencorpora.CorpusReader('annot.opcorpora.xml')

        doc = c.documents(categories='Тема:ЧасКор:В мире')[0]
        tags = doc.categories()
        pass

    def test_connector(self):

        dbrepository = TextDataSetRepository(MongoDbConnector(host='localhost',port=27017, db_name='ml_diploma'))
        dbrepository.save_text_data_set([{},{},{}], True)

    def test_update(self):
        prev_group = {'one': 23, 'two': 45, 'three': 66, 'four': 22, 'five': 32}
        new_words = { 'sec': 44, 'oaoo': 222, 'two': 21, 'two': 65, 'three': 28, 'five': 32}
        max = 30
        expected = { 'oaoo': 222, 'three': 66, 'two': 65,  'two': 45, 'sec': 44, 'five': 32, 'three': 28, 'one': 23,  'four': 22, 'two': 21 }
        result = update_dynamic_feature_group(prev_group, new_words, max)
        pass

    def test_lists(self):
        list1 = [1,2,3,4,5,6,7,8,9,10]
        list2 = [11,22,33,44,55,66,77,88,99,00]

        l1 = list1[:-3]
        l2 = list1[-3:]
        pass

    def test_handle(self):
        dbconnector = MongoDbConnector(host='localhost',port=27017,db_name='ml_diploma')
        dbconnector.connect()
        literature_texts =  list(dbconnector.db['literature_texts'].find({}))
        handler = TextHandler(None, MorphAnalyzer(),0)

        for text in literature_texts:
            obj = type('',(object,),{"words": text['words']})()
            handler.handle(obj)




