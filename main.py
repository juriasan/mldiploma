from executable import *
from Composition2 import *
from FeatureExtractor import *


def print_result(accuracy, name, len):
    print ("------------------------" + name + " CLASSIFIER RESULTS ----------------------------------")
    print("count: %d tp: %d fp: %d tn: %d fn:%d \n" % (len, accuracy['true-positive'],
                  accuracy['false-positive'],
                accuracy['true-negative'], accuracy['false-negative']))

def __main__():

    dbconnector = MongoDbConnector(host='localhost',port=27017,db_name='ml_diploma')
    if dbconnector.connect():
        feature_groups = list(dbconnector.db['feature_groups'].find({}))

       # print ("---------------------------------FEATURES-----------------------------------")
       # for g in feature_groups:
       #     for f in g['feats']:
         #       print(f + "\n")

        lit_texts = list(dbconnector.db['literature_texts'].find({}))
        non_lit = list(dbconnector.db['non_literature_texts'].find({}))

        pos_tag = Tag(lit_texts[0]['expert_tag']['label'])
        neg_tag = Tag(non_lit[0]['expert_tag']['label'])

        train = []
        test = []

        train += lit_texts[:-35]
        train += non_lit[:-400]
        test += lit_texts[-35:]
        test += non_lit[-400:]

        random.shuffle(train)
        random.shuffle(test)

        rules = []
        i = 0
        print("---------------------------------TAG CLASSIFIERS RESULTS---------------------------------")
        all_features = []
        for group in feature_groups:
            all_features += group['feats']
            cls = BinaryClassifier(pos_tag=pos_tag, name=group['name'],
                                   neg_tag=neg_tag, core=NaiveBayes(name='nb',
                                                                    feature_extractor=BagOfWordsExtractor(group['feats'])))
            cls.build(train)
            rules.append(cls)

#            for item in test:
 #               print ("name " + item['name'] + ' e: ' + item['expert_tag']['label'] + ' ne: ' + cls.classify(item))

            result, accuracy = cls.classify_set(test)

            print_result(accuracy, cls.name, len(test))
            i+=1

        simple_area_cls = ClassifiersComposition(pos_tag=pos_tag, name='SIMPLE', neg_tag=neg_tag, core=rules)
        simple_area_cls.build(train)
        result, accuracy = simple_area_cls.classify_set(test)

        print_result(accuracy, simple_area_cls.name, len(test))

        def item_getter(db):
            return dict(list(db['literature_texts_data_set'].find({}))[0]['document_model']['model']).items()

        c_core = []
        for rule in rules:
            c_core.append(rule)
        c_core.append(simple_area_cls)


        complex_cls = BinaryClassifier(name='COMPLEX', pos_tag=pos_tag, neg_tag=neg_tag,
                           core=NaiveBayes('nb', feature_extractor=ComplexExtractor(all_features, simple_area_cls, rules)))
        complex_cls.build(train)
        result, accuracy = complex_cls.classify_set(test)

        print_result(accuracy, complex_cls.name, len(test))

        simple1 = BinaryClassifier(name='EXTRASIMPLE', pos_tag=pos_tag, neg_tag=neg_tag, core=NaiveBayes('nb',
                                                        feature_extractor=
                                                        BagOfWordsExtractor(feats_to_extract=features_top(dbconnector, item_getter))))
        simple1.build(train)

        result, accuracy = simple1.classify_set(test)

        print_result(accuracy, simple1.name, len(test))


if __name__ == '__main__':
    __main__()