from pymongo import *
from abc import *
import operator

class DbConnector(metaclass=ABCMeta):
    _db = None
    _client_data = None
    _client = None
    _db_name = None

    def __init__(self, **kwargs):
        self._client_data = { 'host': kwargs.get('host','localhost'), 'port': kwargs.get('port', 27017)}
        self._db_name = kwargs.get('db_name', "defaultname")

    @abstractmethod
    def connect(self):
        return

    @abstractmethod
    def close(self):
        return

    @property
    def db(self):
        return self._db

    @property
    def client_data(self):
        return self._client_data

    @property
    def db_name(self):
        return self._db_name

class MongoDbConnector(DbConnector):

    def connect(self, force=False):
        if self._client is not None:
            if not force:
                return True

        try:
            self._client = MongoClient(host=self._client_data['host'], port=self._client_data['port'], connect=True)
            self._db = self._client[self._db_name]
        except(ConnectionError):
            return False
        return True

    def close(self):
        if self._client is not None:
            self._client.close()

class Repository(metaclass=ABCMeta):

    _entity_name = None
    def __init__(self, entity_name):
        self._entity_name = entity_name

    @property
    def entity_name(self):
        return self._entity_name

    @entity_name.setter
    def entity_name(self, value):
        self._entity_name = value

    @abstractmethod
    def map_item(self, data_item):
        return

    @abstractmethod
    def map(self, data):
        return
class TextDataSetRepository(Repository):

    _connector_core = None

    def __init__(self, entity_name, core):
        super().__init__(entity_name)
        self._connector_core = core

    def save_data_set(self, data_set, rewrite):
        if self._connector_core is not None:
            if self._connector_core.connect():
                if rewrite:
                    self._connector_core.db[self._entity_name].delete_many({})
                try:
                    item = self.map(data_set)
                    self._connector_core.db[self._entity_name].insert_one(item)
                except Exception as err:
                    self._connector_core.close()
                    self._connector_core.connect(force=True)


    def save_text_data_set(self, data_set, rewrite):
        if self._connector_core is not None:
            if self._connector_core.connect():
                if rewrite:
                    self._connector_core.db[self._entity_name].delete_many({})
                for text_item in data_set.text_data_items:
                    try:
                        self._connector_core.db[self._entity_name].insert_one(self.map_item(text_item))
                    except Exception:
                        self._connector_core.close()
                        self._connector_core.connect(force=True)

    def map(self, data):
        return {
            'document_model':
                {
                    'model': sorted(data.document_model.model.items(), key=operator.itemgetter(1)),
                },
            'words': list(data.words)
        }

    def map_item(self, data_item):
        return {
            'name': data_item.name,
            'document_model':
                {
                    'model':  sorted(data_item.document_model.model.items(), key=operator.itemgetter(1))
                },
            'expert_tag': { 'label': data_item.expert_tag.label },
            'intern_tag': { 'label': data_item.intern_tag.label },
            'words': list(data_item.words)
        }

    def save_text_data_item(self, text_data_item):
        if self._connector_core is not None:
            self._connector_core.db[self._entity_name].insert_one(self.map_item(text_data_item))




