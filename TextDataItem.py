from abc import ABCMeta, abstractmethod, abstractproperty
from opencorpora import  *
from os.path import isfile, join
import os
import random
from queue import Queue
from threading import *
from TextDataHandlers import *
import DocumentModel
class TextDataItem(metaclass=ABCMeta):

    _expert_tag = None
    _document = None
    _document_model = None
    _words = []
    _name = None
    _intern_tag = None

    @property
    def document_model(self):
        return self._document_model

    @document_model.setter
    def document_model(self, value):
        self._document_model = value

    @property
    def document(self):
        return self._document

    @property
    def expert_tag(self):
        return self._expert_tag

    @property
    def words(self):
        return self._words

    @words.setter
    def words(self, value):
        self._words = value

    @property
    def intern_tag(self):
        return self._intern_tag

    @property
    def name(self):
        return self._name

    def __init__(self, expert_tag, intern_tag, document):
        self._document = document
        self._expert_tag = expert_tag
        self._intern_tag = intern_tag
        self._form_words()

    def build(self, document_model):
        self._document_model = document_model
        document_model.form_model(self._words)
        return self

    @abstractmethod
    def _form_words(self):
        return

    def handle_words(self, handler):
        handler.handle(self)
      #  while(handler is not None):
      #      if handler.next is None:
      #          self._words = handler.words
      #      handler = handler.handle(self._words)

class OpencorporaTextDataItem(TextDataItem):

    def __init__(self, expert_tag, intern_tag, document):
        super(OpencorporaTextDataItem, self).__init__(expert_tag=expert_tag, intern_tag=intern_tag, document=document)
        self._name = document.title()

    def _form_words(self):
        self._words = self._document.words() #no implicit logic to_lower



class FileTextDataItem(TextDataItem):
    __path = None

    @property
    def path(self):
        return self.__path

    def __init__(self, expert_tag, intern_tag, path):
        self.__read_file()
        super(FileTextDataItem, self).__init__(expert_tag=expert_tag, intern_tag=intern_tag, document=None)
        self.__path = path
        self._name = self.__path

    def __handle_line(line):
        return line.split(' ')

    def __read_file(self):
        file = None
        try:
            file = open(self.__path, encoding='utf8')
        except IOError:
            pass
        finally:
            self._document = file

    def _form_words(self):
        words = self._words
        for line in self._document:
            for word in self.__handle_line():
                words.append(word)
        self._words = words


class TextDataSet(metaclass=ABCMeta):
    _document_model = None
    #__text_document_models = []
    _text_data_items = []
    _core = None
    _words = []
    _queue = Queue()
    _handlers_max_num = 25
    _handlers_min_num = 5
    _morph_analyzer = None
  #  _new_indexes = []

    @property
    def document_model(self):
        return self._document_model

    @property
    def text_data_items(self):
        return self._text_data_items

    @property
    def count(self):
        return len(self._text_data_items)

    def __init__(self, document_model):
        self._document_model = document_model

    def handle_words(self, morph_analyzer):
        text_items = self._text_data_items
      #  if beginInd is not None and endInd is not None:
     #       text_items = self._text_data_items[beginInd:endInd + 1]

        for text_item in text_items:
            self._queue.put(text_item)
        num_of_threads = int(len(text_items) / self._handlers_min_num) if len(text_items) >= self._handlers_min_num else len(text_items)
        if num_of_threads > self._handlers_max_num:
            num_of_threads = self._handlers_max_num

        for ind in range(num_of_threads):
            handler = TextHandler(queue=self._queue, morph_analyzer=morph_analyzer, num=ind)
            handler.daemon = True
            try:
                handler.start()
            except(KeyboardInterrupt, SystemExit):
                handler.stop = True
                raise

        while(self._queue.unfinished_tasks > 0):
            pass
      #  self._queue.join()
    #    for dataitem in self._text_data_items:
   #         dataitem.handle_words(handler=handler)
        return self

    def build(self, prototype):
        if self._text_data_items == None or len(self._text_data_items) < 1 or self._text_data_items == None:
            return
        for dataitem in self._text_data_items:
            dataitem.build(prototype())
            self._words += dataitem.words

        self._document_model.form_model(self._words)
        return self

    @abstractmethod
    def extract_data_items(self, expert_tag, tags):
        return

    @abstractmethod
    def save(self, dbconnector, rewrite):
        return

    def extract_data(self, expert_tag, limit=None, tags=None):
        i = 0

        self._text_data_items.clear()
        self.extract_data_items(expert_tag, tags)

        if limit == None:
            limit = len(self._text_data_items)

        self._text_data_items = self._text_data_items[:limit]
        random.shuffle(self._text_data_items)

        return self

    @property
    def core(self):
        return self._core

  #  def append(self, text_data_set):
    #    if text_data_set == None or len(text_data_set) < 1:
    #        return

    #    if text_data_set[0].__class__ == self.__class__:
    #        self._text_data_items += text_data_set
    #    return self

    @property
    def words(self):
        return self._words

class OpenCorporaDataSet(TextDataSet):

    def __init__(self, core, document_model):
        super(OpenCorporaDataSet, self).__init__(document_model)
        self._core = core

    def extract_data_items(self, expert_tag, tags=None):
        if tags != None:
            for tag in tags:
                self._text_data_items += [OpencorporaTextDataItem(expert_tag=expert_tag, intern_tag=DocumentModel.Tag(tag), document=doc)
                                          for doc in self._core.documents(categories=tag)]
        return self

    def save(self, dbrepository, rewrite):
        dbrepository.save_text_data_set(data_set=self, rewrite=rewrite)
        return self

    def save_set(self, dbrepository, rewrite):
        dbrepository.save_data_set(data_set=self, rewrite=rewrite)
        return self

class FileDataSet(TextDataSet):

    def __init__(self, path_to_dir, document_model):
        super(FileDataSet,self).__init__(document_model=document_model)
        self._core = [os.path.abspath(os.path.join(path_to_dir, f)) for f in os.listdir(path_to_dir) if isfile(join(path_to_dir, f))]

    def extract_data_items(self, expert_tag, intern_tag, tags=None):
        for file in self._core:
            self._text_data_items.append(FileTextDataItem(expert_tag=expert_tag, intern_tag=DocumentModel.Tag(intern_tag), path=file))
        return self
    def save(self, dbconnector, rewrite):
        pass
        return self



