from abc import ABCMeta
from enum import Enum
from TextDataItem import *
import operator

class DocumentModel(metaclass=ABCMeta):

    _model = None

    @property
    def model(self):
        return self._model

    @abstractmethod
    def form_model(self, document):
        return

class BagOfWords(DocumentModel):

    def __init__(self, model=None):
        self._model = defaultdict(lambda : 0)
        if model is not None:
            self._model = model

    def form_model(self, words):
        self._model.clear()
        for word in words:
            self._model[word] += 1

class Tag:

    __label = None

    def __init__(self, label):
        self.__label = label

    @property
    def label(self):
        return self.__label

class FeaturedBagOfWords(BagOfWords):

    __filter_features = []

    def __init__(self, filter_features, model=None):
        super().__init__(model)
        self.__filter_features = filter_features

    @property
    def filter_features(self):
        return self.__filter_features

    def form_model(self, words):
         super().form_model(words)
         self._model =\
             sorted({key:value for key, value in self._model if key in self.__filter_features}.items(), key=operator.itemgetter(1))
         self.get_features()