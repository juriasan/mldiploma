from abc import *
from collections import defaultdict

class FeatureExtractor(metaclass=ABCMeta):

    _feats_to_extract = None
    def __init__(self, feats_to_extract):
        self._feats_to_extract = feats_to_extract

    @abstractmethod
    def extract(self, text):
        return


class BagOfWordsExtractor(FeatureExtractor):

    def extract(self, text):
        features = []
        document_model = dict(text['document_model']['model'])

        for key in self._feats_to_extract:
            if key in document_model.keys():
                features.append(document_model[key])
            else:
                features.append(0)
        return features

class ComplexExtractor(BagOfWordsExtractor):

    _simple_area_cls = None
    _tags_cls = None
    _results =  defaultdict(lambda: defaultdict(lambda: ' '))

    def __init__(self, features_to_extract, simple_area_cls, tags_cls):
        super().__init__(feats_to_extract=features_to_extract)
        self._tags_cls = tags_cls
        self._simple_area_cls = simple_area_cls

    def extract(self, text):
        features = []

        if self._simple_area_cls is None or self._tags_cls is None:
            return features

        #features += super().extract(text)

        if self._simple_area_cls is not None:
            features.append(self._simple_area_cls.classify(text))

        if self._tags_cls is not None:
            for tag_cls in self._tags_cls:
                features.append(tag_cls.classify(text))
        return features
